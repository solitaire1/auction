<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class callender extends Model {

    public function up() {
        Schema::create('callender', function (Blueprint $table) {
            $table->increments('id');
            $table->string('json_file');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('callender');
    }

}
