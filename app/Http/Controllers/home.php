<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\callender;
use App\auction;
use Illuminate\Support\Facades\DB;

class home extends Controller {

    public function upload_callender_data() {

        return view('file_upload');
    }

    public function save_callender_data(Request $request) {


        $validatedData = $request->validate([
            'fileToUpload' => 'required',
        ]);
        if ($validatedData->fails()) {

            die("asd");
            return redirect('/')->with('error', "invalid file");
        } else {
            die("dsf");
        }

        $callender = new callender();

        if ($request->hasFile('fileToUpload')) {
            $json_file = $request->file('fileToUpload');
            $path = public_path() . '/callender/';
            $filename = time() . '.' . $json_file->getClientOriginalExtension();
            $json_file->move($path, $filename);

            $callender->json_file = 'public' . '/callender/' . $filename;
        }

        $callender->save();
    }

    public function show_callender_files() {
        $callender_table = callender::get();

        return view('callender_data')->with('callender', $callender_table);
    }

    public function save_data_auction_time_date($id) {
        $callender_table = callender::find($id);

        $json_data = file_get_contents(base_path($callender_table['json_file']));
        $auction_data = json_decode(json_decode($json_data));
        $saleList_data = $auction_data->data->saleList;
//    echo"<pre>";
//    print_r($saleList_data);
//    echo"</pre>";
//        die("sdf");

        foreach ($saleList_data as $details) {
            $auction = new auction();
            $auction->startTime = $details->startTime;
            $auction->saleName = $details->saleName;
            $auction->regionName = $details->regionName;
            $auction->currentSaleTimeEpoch = $details->currentSaleTimeEpoch;
            $auction->yard_number = $details->yards[0]->number;
            $auction->yard_name = $details->yards[0]->name;
            $auction->nextSaleTimeEpoch = $details->nextSaleTimeEpoch;
            $auction->auctionDate_dateAsInt = $details->auctionDate->dateAsInt;
            $auction->auctionDate_date = $details->auctionDate->date;
            $auction->saleTime = $details->saleTime;
            $auction->isLive = $details->isLive;
            $auction->status = 1;
            $auction->save();
        }

        return redirect('auction_data_display');
//        return redirect()->route('');
    }

    public function lots_search() {


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.copart.com/public/lots/search?filter%5BMISC%5D=auction_host_id%3A151%2Cauction_date_utc%3A%5B%222019-04-18T00%3A00%3A00Z%22%20TO%20%222019-04-18T23%3A59%3A59Z%22%5D%2Cauction_date_type%3AF&sort=auction_date_type%20desc%2Cauction_date_utc%20asc",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json, text/javascript, */*; q=0.01",
                "authority: www.copart.com",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "origin: https://www.copart.com",
                "postman-token: 9f79b013-61f8-a276-6ef9-238fdba04c60",
                "referer: https://www.copart.com/saleListResult/151/2019-04-18?location=CA%20-%20Antelope&saleDate=1555614000000&liveAuction=false&from=&yardNum=151",
                "scheme: https",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
                "x-requested-with: XMLHttpRequest",
                "x-xsrf-token: 3b4b429e-3860-4227-afd1-5065511d15d0"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {

            echo"<pre>";
            print_r(json_decode($response));
            echo"</pre>";
        }
    }

    public function auction_data_display() {
//         $condition = ['isLive' => 1];
        $auction_data = auction::where('isLive', '=', 1)->get();

        return view('auction_data')->with('auction_data', $auction_data);
    }

    public function parameter_to_get_lots() {

//        print_r($_POST);die("dsf");
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.copart.com/public/lots/search?filter%5BMISC%5D=auction_host_id%3A151%2Cauction_date_utc%3A%5B%222019-04-18T00%3A00%3A00Z%22%20TO%20%222019-04-18T23%3A59%3A59Z%22%5D%2Cauction_date_type%3AF&sort=auction_date_type%20desc%2Cauction_date_utc%20asc",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json, text/javascript, */*; q=0.01",
                "authority: www.copart.com",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "origin: https://www.copart.com",
                "postman-token: 9f79b013-61f8-a276-6ef9-238fdba04c60",
                "referer: https://www.copart.com/saleListResult/151/2019-04-18?location=CA%20-%20Antelope&saleDate=1555614000000&liveAuction=false&from=&yardNum=151",
                "scheme: https",
                "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
                "x-requested-with: XMLHttpRequest",
                "x-xsrf-token: 3b4b429e-3860-4227-afd1-5065511d15d0"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $lots_list = json_decode($response);
            $totalElements = $lots_list->data->results->totalElements;
        }


        if (isset($totalElements) && !empty($totalElements)) {

            //      same curl with all data count   

            $curl_next = curl_init();

            curl_setopt_array($curl_next, array(
                CURLOPT_URL => "https://www.copart.com/public/lots/search?filter%5BMISC%5D=auction_host_id%3A151%2Cauction_date_utc%3A%5B%222019-04-18T00%3A00%3A00Z%22%20TO%20%222019-04-18T23%3A59%3A59Z%22%5D%2Cauction_date_type%3AF&sort=auction_date_type%20desc%2Cauction_date_utc%20asc&size=" . $totalElements,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json, text/javascript, */*; q=0.01",
                    "authority: www.copart.com",
                    "cache-control: no-cache",
                    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                    "origin: https://www.copart.com",
                    "postman-token: 9f79b013-61f8-a276-6ef9-238fdba04c60",
                    "referer: https://www.copart.com/saleListResult/151/2019-04-18?location=CA%20-%20Antelope&saleDate=1555614000000&liveAuction=false&from=&yardNum=151",
                    "scheme: https",
                    "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36",
                    "x-requested-with: XMLHttpRequest",
                    "x-xsrf-token: 3b4b429e-3860-4227-afd1-5065511d15d0"
                ),
            ));

            $response_next = curl_exec($curl_next);
            $err_next = curl_error($curl_next);

            curl_close($curl_next);

            if ($err_next) {
                echo "cURL Error #:" . $err;
            } else {
                $lots_list_next = json_decode($response_next);

                $sales_list = $lots_list_next->data->results->content;
               $data='';
                foreach($sales_list as $data){
//                    print_r($data->mkn);die;
                        $ln = $data->ln;
                        $mkn = $data->mkn;
//                        echo $mkn;
                        $lm = $data->lm;
//                        echo $lm;
                        $lcy = $data->lcy;
                        $aan = $data->aan;
                        $tims = $data->tims;
                        $yn = $data->yn;
                        $gr = $data->gr;
                        $al = $data->al;
                        $ad = $data->ad;
                        $dd = $data->dd;
                        $la = $data->la;
                        $stt = $data->stt;
                        $ts = $data->ts;
                        $orr = $data->orr;
               
                    DB::table('saleslist')->insert(
                        ['orr' => $orr,'ts' => $ts,'stt' => $stt,'la' => $la,'dd' => $dd,'ad' => $ad,'al' => $al,'gr' => $gr,'yn' => $yn,'yn' => $yn,'tims' => $tims,'aan' => $aan,'ln' => $ln ,'mkn' => $mkn, 'lm' => $lm,'lcy' => $lcy,]
                        
                );
                   
                }
                
                

                die("ddsfd");
            }
        }
    }

}
