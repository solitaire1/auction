<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
       
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>

        <div class="container">
            <h2>Auction Calendar data</h2>
            <table class="table" id='myTable'>
                <thead>
                    <tr>
                        <th>Sale Name</th>
                        <th>Region Name</th>
                        <th>Yard Number</th>
                        <th>Yard Name</th>
                        <th>NextSale TimeEpoch</th>
                        <th>AuctionDate dateAsInt</th>
                        <th>AuctionDate date</th>
                        <th>Current Sale Time Epoch</th>
                        <th>Sale Time</th>
                        <th>Start Time</th>
                        <th>Is live</th>
                        <th>Save lots Sale</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($auction_data as $data)
                    <tr>
                        <th>{{$data['saleName']}}</th>
                        <th>{{$data['regionName']}}</th>
                        <th>{{$data['yard_number']}}</th>
                        <th>{{$data['yard_name']}}</th>
                        <th>{{$data['nextSaleTimeEpoch']}}</th>
                        <th>{{ date('d M Y', strtotime($data['auctionDate_dateAsInt'])) }}</th>
                        <th>{{ date('d M Y', strtotime($data['auctionDate_date']))}}</th>
                        <th>{{ date("d M Y  H:i:s ", $data['currentSaleTimeEpoch'])}}</th>
                        <th>{{ date('h:i:s', strtotime($data['saleTime']))}}</th>
                        <th>{{$data['startTime']}}</th>
                        <th>{{$data['isLive']}}</th>
                        <th><button class='get-lot-sale' data-islive='{{$data['isLive']}}' data-yardno='{{$data['yard_number']}}' data-dateAsInt='{{$data['auctionDate_dateAsInt']}}' >Lots Sale</button></th>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

    </body>
    <script>
$(document).ready(function () {
    $('#myTable').DataTable();
});
    </script>
    <script>
        $(document).ready(function () {
            jQuery('.get-lot-sale').click(function (e) {
                var yardno = $(this).attr("data-yardno");
                var dateAsInt = $(this).attr("data-dateAsInt");
                var islive = $(this).attr("data-islive");
             alert("dsf");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN':'<?php echo csrf_token() ?>'
                    }
                });

                jQuery.ajax({
                    url: "{{ url('parameter_to_get_lots')}}",
                    method: 'post',
                    data: {
                        yardno: yardno,
                        dateAsInt: dateAsInt,
                        islive: islive
                    },
                    success: function (result) {
                        alert("dsf");
                    }});
            });


        });
    </script>



</html>
