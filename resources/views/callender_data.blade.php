<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h2>Auction Callender data</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>Json File Path</th>
                        <th>Created date</th>
                        <th>view</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($callender as $data)
                    <tr>
                        <td>{{ $data['json_file']}}</td>
                        <td>{{$data['created_at'] }}</td>
                        <td><a href="{{ url('save_calender_info/'.$data['id']) }}"><button type="button" class="btn btn-info">Save Data In Database</button></a></td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

    </body>
</html>
