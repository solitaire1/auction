<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('upload_data', 'home@upload_callender_data');
Route::get('show_callender_files', 'home@show_callender_files');
Route::post('save_callender_data', 'home@save_callender_data');
Route::post('parameter_to_get_lots', 'home@parameter_to_get_lots');
Route::any('save_calender_info/{id}', 'home@save_data_auction_time_date');
Route::get('auction_data_display', 'home@auction_data_display');
Route::get('lots_search', 'home@lots_search');

